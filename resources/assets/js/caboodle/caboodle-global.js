window.mdc.autoInit(/* root */ document, () => {})
// RE-INIT MDC
function reInitMDC() {
    $('.mdc-button').each(function() {
        // console.log($(this)[0]);
        // console.log(document.querySelector('.mdc-button'))
        mdc.ripple.MDCRipple.attachTo($(this)[0]);
    });
};
function reInitTooltips() {
    $('[data-toggle="tooltip"]').tooltip({ trigger: "hover"});
};


observeDOM(document.getElementById('body'), function () {
    reInitTooltips();
    reInitMDC();
});

//---set the input cursor position-----//
$.fn.setCursorPosition = function (pos) {
    this.each(function (index, elem) {
        if (elem.setSelectionRange) {
            elem.setSelectionRange(pos, pos);
        } else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    });
    return this;
};
//---get the input current cursor position-----//
$.fn.getCursorPosition = function () {
    var el = $(this).get(0);
    var pos = 0;
    if ('selectionStart' in el) {
        pos = el.selectionStart;
    } else if ('selection' in document) {
        el.focus();
        var Sel = document.selection.createRange();
        var SelLength = document.selection.createRange().text.length;
        Sel.moveStart('character', -el.value.length);
        pos = Sel.text.length - SelLength;
    }
    return pos;
}

//---- get element's outer html
$.fn.outerHTML = function (s) {
    return s
        ? this.before(s).remove()
        : jQuery("<p>").append(this.eq(0).clone()).html();
};

	/* REDACTOR */
function initRedactor(){
	var redactor = $('.redactor');
	if (redactor.length > 0) {
		var redactorUpload = redactor.data('redactor-upload');
		var token = redactor.closest('form').find('input[name="_token"]').val();
		redactor.redactor({
			minHeight: '200px',
			fileUpload: redactorUpload + '?_token=' + token,
			imageUpload: redactorUpload + '?_token=' + token,
			imageResizable: true,
			plugins: ['table', 'fontcolor', 'fontsize', 'alignment', 'counter']
		});
	}
}
	/* REDACTOR END */
var selected_orders = [];
function orderSelect() {
	$('.order-select').on('click', function() {
		$this = $(this);
		if (selected_orders.includes($this.data('id'))) {
			$this.removeClass('selected');
			
			var index = selected_orders.findIndex(order => {
				return order === $this.data('id')
			});
			selected_orders.splice(index, 1);
			$this.find('.order-checkbox').attr('checked', false);
		} else {
			$this.addClass('selected');
			selected_orders.push($this.data('id'));
			$this.find('.order-checkbox').attr('checked', true);
		}
		$('.orders_id').val(selected_orders.toString());
	});
}
$('#print-orders').on('submit', function(e) {
	if ($('.orders_id').val() === '') {
		e.preventDefault();
	}
});

function checkWindowWidth() {
	if ($(window).width() < 1032) {
		$('body').addClass('tab-menu');
	} else {
		$('body').removeClass('tab-menu');
	}
	if ($(window).width() < 1032) {
		hideMenu();
	} else {
		showMenu();
	}
}

function showMenu() {
	$('body').removeClass('hide-menu');
}
function hideMenu() {
			$('body').addClass('hide-menu');
			$('.collapse-list').collapse('hide');
}

$(document).ready(function() {
	/* parsley FORM EVENTS */
	orderSelect();
    var parsleyForm = $('.form-parsley');
	if (parsleyForm.length > 0) {
		parsleyForm.submit(function(e) {
			e.preventDefault();
        });
        parsleyForm.each(function(e) {
			var formElement = $(this);
            $(this).parsley().on('form:submit', function () {
				console.log('submit');
				var toasterMessage = parsleyForm.hasClass("form-delete") ? 'Deleting data' : 'Saving data';
                showLoader(true);
                showNotifyToaster('info', '', '<i class="far fa-circle-notch fa-spin" style="margin-right: 5px"></i>' + toasterMessage);

                var url = formElement.attr('action');
                var data = formElement.serialize();
                var method = formElement.find('input[name="_method"]').val();
                var type = (_.isUndefined(method) ? 'POST' : method);
                submitForm(url, data, type, formElement);

                return false;
            });
        });
		
	}
	var navbar = $('.navbar');
	//===== SIDEBAR MANAGEMENT
	$('aside>ul>li>a').on('click', function() {
		$('aside>ul>li').removeClass('open');
		var link = $(this);
		var list = link.closest('li');
		if (list.hasClass('has-list')) {
			list.toggleClass('open');
		}
	});
	navbar.find('.menu-btn').on('click', function() {
		$('body').toggleClass('hide-menu');
	});
	$(window).resize(function() {
		checkWindowWidth();
		boxElementFromWidth();
	});
	
	$('.form-parsley-submit').click(function(e) {
        e.preventDefault();
	    $('.form-parsley').submit();
	});
	
	$('.form-seo-submit').click(function(e) {
        e.preventDefault();
	    $('.form-seo').submit();
	});
	/* parsley FORM EVENTS -- END*/

	/* LAYOUT INITS */
	var navbar = $('.navbar');
	$('.scrollbar-macosx').scrollbar();
	navbar.find('.menu-btn').on('click', function() {
		$('body').toggleClass('hide-menu');
	});
	$('.collapse-list').on('show.bs.collapse', function () {
	    $('.collapse-list').collapse('hide');
	});
	$('body').click(function(e) {
		if ($(this).hasClass('hide-menu') && $(window).width() >= 768) 
		    $('.collapse-list').collapse('hide');
	});

	$(window).resize(function() {
		checkWindowWidth();
	});

	checkWindowWidth();
    /* LAYOUT INITS -- END*/

	/* REDACTOR */
	var redactor = $('.redactor');
	if (redactor.length > 0) {
		var redactorUpload = redactor.data('redactor-upload');
		var token = redactor.closest('form').find('input[name="_token"]').val();
		redactor.redactor({
			minHeight: '200px',
			fileUpload: redactorUpload + '?_token=' + token,
			imageUpload: redactorUpload + '?_token=' + token,
			imageResizable: true,
			plugins: ['table', 'fontcolor', 'fontsize', 'alignment', 'counter']
		});
	}
	/* REDACTOR END */

	$(".select2").select2({
		selectOnClose: true
	  });


	/* DELETE MODAL */
	$('.toggle-delete-all').on('click', function() {
		var checkbox = $(this);
		var toggle = (checkbox.is(':checked')) ? true : false;
		$('input[name="ids[]"]').prop('checked', toggle);
	});
	$('#delete-modal').find('input[type=checkbox]').on('click',function(){
		if($(this).is(':checked')){
			$('#delete-modal').find('.btn-delete').removeAttr('disabled','disabled');
		}else{
			$('#delete-modal').find('.btn-delete').attr('disabled','disabled');
		}
	});
	$('#delete-modal').find('.btn-delete').on('click', function() {
		if(!$(this).is(':disabled')){
			$(this).attr('disabled','disabled');
			$('.form-delete').submit();
		}
    });
	/* DELETE MODAL END */

	// DATA TOGGLE ALERT

	$('[data-toggle-alert]').click(function(e) { 
        e.preventDefault();
		var	options = {
            type: $(this).data("toggle-alert") ? $(this).data("toggle-alert") : 'warning', // ---- alert type
            title: $(this).data("alert-title") ? $(this).data("alert-title") : 'Confirm Action', //--- alert title
            text: $(this).data("alert-message") ? $(this).data("alert-message") : 'Are you sure you want to delete selected item/s?', // ----- alert message
            showCancelButton: $(this).data("alert-with-cancel") !== undefined ? $(this).data("alert-with-cancel") : true, // ---- show cancel button option
            confirmButtonText: $(this).data("alert-confirm-button-text") !== undefined ? $(this).data("alert-confirm-button-text") : 'Confirm', // ---- confirm button custom text
            confirmButtonClass: $(this).data("alert-confirm-button-class") !== undefined ? $(this).data("alert-confirm-button-class") : '',
            cancelButtonText: $(this).data("alert-cancel-button-text") ? $(this).data("alert-cancel-button-text") : 'Cancel', // ----- cancel button custom text
            closeOnConfirm: $(this).data('alert-close-on-confirm') !== undefined ? $(this).data('alert-close-on-confirm') : true, // ----- close alert on confirm
            disableButtonsOnConfirm: $(this).data('alert-disable-on-confirm') !== undefined ? $(this).data('alert-disable-on-confirm') : true, // ---- disable confirm button on click
        };
		var formToSubmit = $(this).data('alert-form-to-submit') !== undefined ? $(this).data('alert-form-to-submit') : false; //--- form to submit on confirm (if classname is enetered, make sure to include '.' in the name, same with id)
        var actionEl = $(this);

		swal(
			options,
			function(confirm) {
			if(confirm) {
				if(formToSubmit) {
                    var url = actionEl.attr('href');
                    var method = actionEl.attr('method');
                    if (url != '' && !_.isUndefined(method) && method != '') {
                        showLoader(true);
                        var notifTitle = (_.isUndefined(actionEl.data('notif-title')) ? '' : actionEl.data('notif-title'));
                        var notifMessage = (_.isUndefined(actionEl.data('notif-message')) ? '' : actionEl.data('notif-message'));
                        if (notifTitle != '' || notifMessage != '') {
                            showNotifyToaster('info', notifTitle, notifMessage);
						};
						
						var data = $(formToSubmit).serialize();

						if(actionEl.data('additionals')) {
							_.each(actionEl.data('additionals').split(','), function(additional) {
								var dataSplit = additional.split(':');

								data += '&' + dataSplit[0] + '=' + dataSplit[1];
							});
						}
						
						data = data.replace('_method=DELETE', '_method=' + method.toUpperCase());
						// console.log($(formToSubmit));
						// console.log(data);
                        submitForm(url, data, method);
                    } else {
						$(formToSubmit).submit();
                    }
				} else {
					if(actionEl.attr("link")) {
						window.location.href = actionEl.attr("link");
					}
				}
			}
		});
	});
    
    $('form.unloadpagevalidator').each(function () {
        $(this).unloadpagevalidator({
            customizeWarning: true, // enabled function customizeWarningOnClickOfLink when true
            customizeTitle: "Do you want to reload this site?",// title of custom alert
            customizeText: "Changes you made may not be saved.",// text of custom alert
            customizeType: "warning", // type of custom alert (warning, success, info, error)
            customizeTextStay: "Stay on this page", // text of button stay
            customizeTextLeave: "Leave this page", // text of button leave
            customizeBtnColorLeave: "#2ac3f2", // color of leave page button
            customClass: 'sweetalert', // customClass for sweetalert
            targetForm: 'form', // target form to be only set if not use in jquery chaining *this value will be ignored when use chaining method 
            targetInputs: 'input, textarea', // inputs to validate inside targetForm
            classUnloadvalidation: 'unloadvalidation', // default class that will add on keydown/change of targetInputs inside targetForm
            warningForAjax: true, // enabled function warningDuringAjaxCall when true
            disablingDuringAjax: true // enabled function disablingButtonOnAjaxCall when true
        });
    });

	// ANIMATED PLACEHOLDER EVENT LISTENER
	$(document).on(".animated-placeholder input", "input", function() {
		if($(this).val()) {
			$(this).parent().addClass("filled");
		} else {
			$(this).parent().removeClass("filled");
		}
	});

	// money input
	$(document).on("input", ".money-format-input", function() {
		var _this = $(this);
		var _thisValue = _this.val().replace(/[^\d.-]/g, '');
		if(isNaN(_thisValue)) {
	        _this.val(parseFloat(_thisValue));

	        if(_thisValue == "NaN") {
	            _this.val(0);
	        }
	    } else {
			_this.val(_thisValue);
		}
	});

	//form fields dependencies: hide||show on change
	$(document)
        .on("change", "[hidden-target]", function() {
            var _this = $(this);
            var value = _this.val();
            var type = _this.attr("type");
            var stateValue = ''; //--- trigger value that checks the target's field value.
            var stateBool = false; //--- trigger value that checks the target's field state. I.g. check if the target is checked||selected
            var targets = $(_this.attr("hidden-target"));

            $.each(targets, function(i, target) {
                var targetValueTrigger = $(this).attr("trigger-value");
                var targetStateTrigger = $(this).attr("trigger-state");
                var triggerCondition = $(this).attr("trigger-condition");

                if(type === "checkbox" || type === "radio") {
                    stateValue = _this[0].checked ? "checked" : "unchecked";
                    stateBool = _this[0].checked;
                }
                if(targetValueTrigger !== undefined) {
                	var valueState = false;
                	var splitValue = targetValueTrigger.split("|");
                	
                	$.each(splitValue, function(k, v) {
                		if(v == value) {
                			valueState = true;
                			return false;
                		}
                	});

                    if((valueState && type !== "radio" && type !== "checkbox") || (valueState && type === "radio" && _this[0].checked)) {
                        if(triggerCondition == "not") {
                            $(this).hide();
                        } else {
                            $(this).show();
						}
						changeRequirements($(this), true);

                    } else {
                        if(triggerCondition == "not") {
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
						changeRequirements($(this), false);

                    }
                } else if(targetStateTrigger !== undefined) {
                    if(targetStateTrigger == stateValue || targetStateTrigger.toString() == stateBool.toString()) {
                        if(triggerCondition == "not") {
							$(this).hide();
							changeRequirements($(this), false);
                        } else {
							$(this).show();
							changeRequirements($(this), true);
                        }

                    } else {
                        if(triggerCondition == "not") {
                            $(this).show();
                        } else {
                            $(this).hide();
                        }
						changeRequirements($(this), false);

                    }   
                } 
            });
        })
        //-- for input fields with invalid class
        .on("focusin", ".invalid, .animated-placeholder input", function() {
        	var _this = $(this);
        	if(_this.parents(".animated-placeholder").length > 0) {
        		_this.parents(".animated-placeholder").removeClass("invalid");
        	}

        	_this.removeClass("invalid");
        });

        $("[hidden-target]").change();

        $(".animated-placeholder").each(function() {
        	$("input", this).prop("autocomplete", "off");
        });

        //--- COLOR PICKER INput
        $(document)
	        .on("click", ".color-input .selected-color", function() {
	        	$(this).siblings(".color-palette-selector").click();
	        })
	        .on("change", ".color-palette-selector", function() {
	        	var _this = $(this);
	        	var color = _this.val();
	        	var dataColor = _this.data("value");

	        	if(color && dataColor) {
		        	_this.siblings(".selected-color").css("background", color);
		        	_this.siblings(".color-text-value").val(color);
		        	_this.data("value", color);
	        	} else {
	        		_this.data("value", "none");
	        	}
	        })
	        .on("input", ".color-text-value", function() {
	        	var _this = $(this);
	        	var color = _this.val();

	        	if(color[0] !== '#' && validTextColour(color)) {
	        		var hex = colorToHex(color);
	        		if(hex) {
	        			_this.val(hex);
	        		}
	        	}

	        	if(!color)
	        		_this.siblings(".selected-color").css("background", "#fff");
	        });

	    $(".color-palette-selector").change();

	    var imageDataContainer = "";
	   	$(document)
		   	.on("click", ".hidden-image-selector-btn", function() {
	        	imageDataContainer = $(this).parents(".image-data-container");
	        	$("#hiddenImageSelector .sumo-asset-select-button").click();
	        })
	        //--- preview image
	        .on("click", ".image-display-container .preview-image", function() {
	        	var selectedImage = $(this).parents(".image-display-container").find("img").attr("src");
	        	$.fancybox.open([{src: selectedImage}], { loop : false });
			})
			.on("click", ".image-lightbox-preview", function() {
	        	var selectedImage = $(this).find("img").attr("src");
	        	$.fancybox.open([{src: selectedImage}], { loop : false });
	        })
	        //--- remove image
	        .on("click", ".image-display-container .remove-image", function() {
	        	var imageDataContainer = $(this).parents(".image-data-container");
	        	var imageDisplay = imageDataContainer.find(".image-display-container");

	        	imageDisplay.find("img").attr("src","");
	        	imageDisplay.addClass("hide");
				imageDataContainer.find(".empty-message").removeClass("hide");
				imageDataContainer.find(".hide-on-select").removeClass("hide");
	        	imageDataContainer.find(".image-data").val("").change();
	        });

		    $("#assets-modal .btn-select").click(function() {
		    	if(imageDataContainer) {
			    	imageDataContainer.find(".image-data").val($("[name=fake_image]").val()).change();
			    	imageDataContainer.find(".image-display-container img").attr("src", $("#hiddenImageSelector").find(".sumo-asset-img-container img").attr("src"));
					imageDataContainer.find(".empty-message").addClass("hide");
					imageDataContainer.find(".hide-on-select").addClass("hide");
			    	imageDataContainer.find(".image-display-container").removeClass("hide");
			    	imageDataContainer = "";	
		    	}
			});

		//custom collapse container
		if ($("[collapse-target]").length > 0) {
			$.each($("[collapse-target]"), function() {
				var targetCollapse = $(`[collapse-trigger="${$(this).attr("collapse-target")}"]`),
					collapseStatus = targetCollapse.attr("collapse-status");

				$(this).append(`
					<div class="coltri-btn ${collapseStatus == 'collapsed' ? '' : 'in'}" role="button">
						<i class="fas fa-caret-circle-down"></i>
					</div>
				`);

				targetCollapse.html(`<div class="collapse-target-container margin-top-10">${targetCollapse.html()}</div>`);
			});

			$(document)
				.on("click", ".coltri-btn", function() {
					var target = $(this).parents("[collapse-target]").attr("collapse-target"),
						targetCollapse = $(`[collapse-trigger="${target}"]`),
						targetHeight = 0;
					
					if(targetCollapse.height() == 0) {
						targetHeight = targetCollapse.find(".collapse-target-container").outerHeight(true);
					}

					if(targetHeight > 0) {
						$(this).addClass("in");
					} else {
						$(this).removeClass("in");
					}

					targetCollapse.animate({
						"max-height": targetHeight
					}, 500);
				});
		}

		if ($(".caboodle-tab").length > 0) {
			$(document)
				.on("click", ".caboodle-tab .tab-pill", function() {
					var caboodleTab = $(this).parents(".caboodle-tab"),
						target = $(this).attr("tab-target");

					$(this)
						.addClass("active")
						.siblings(".active")
							.removeClass("active");

					caboodleTab
						.find(".tab.show")
							.removeClass("show");

					caboodleTab
						.find(`[tab=${target}]`)
							.addClass("show");
				});
		}
		
		//--USER PROFILE IMAGE COMPONENT
		$.each($('.user-profile-image'), function() {
			var _this = $(this);

			//-- append image controls
			_this.find('.sumo-asset-img-container').append(`<div class="controls-overlay">
							<i class="icon-btn far fa-image"></i>
						</div>`);
		});	

		if($('.user-profile-image').length > 0) {
			$(document).on('click', '.controls-overlay', function() {
				$(this)
					.parents('.user-profile-image')
						.find('label')
							.find('span')
								.click();
			});
		}
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});
});

/* SUBMIT parsley FORM 
    @param url: request url
    @param data: serialized data of form
    @param type: request method
    @param formElem: form element target
*/
function submitForm(url, data, type, formElem) {
	$.ajax({
		type : type,
		url: url,
		data : data,
		dataType : 'json',
		header : {unLoadWarningIgnore : false},
		processData: false,
		success : function(data) {
			var title = data.notifTitle;
			var message = (_.isUndefined(data.notifMessage) ? '' : data.notifMessage);
			var status = (_.isUndefined(data.notifStatus) ? 'success' : data.notifStatus);
			
			if (status != '' && (title != '' || message != '')) {
				toastr.clear();
				showNotifyToaster(status, title, message);
			}

			if (data.resetForm) {
				resetForm(formElem);
			}

			if (! _.isUndefined(data.redirect)) {
				setTimeout(function() {
					window.location = data.redirect;
				}, 1500);
			} else {
                showLoader(false);
            }
			try {
                sumoFormSuccessHandler(data, formElem);
			} catch(e) {}
		},
		error : function(data, text, error) {
			showLoader(false);
			var message = '';
			_.each(data.responseJSON, function(val) {
				message += val + ' ';
			});
			toastr.clear();
			showNotifyToaster('error', 'Error saving.', message);
		}
	});
};
/* SUBMIT parsley FORM -- END */

/* RESET FORM */
function resetForm(form) {
    form.parsley().reset();
    // form.find('input').val('');
    // form.find('textarea').val('');
};
/* RESET FORM  -- END*/

/* SHOW LOADER */
function showLoader(val) {
	$('#page-loader').toggleClass('hide', !val);
};
/* SHOW LOADER -- END*/

/* SHOW NOTIFY TOASTER 
	@param status
	@param title
	@param message
	@param timer = 0 to disable the timer
*/
function showNotifyToaster(status, title, message, timer) {
	var message = (_.isUndefined(message) ? null : message);
	var timer = (_.isUndefined(timer) ? 3000 : timer);

	toastr.options = {
		"closeButton": true,
		"debug": false,
		"newestOnTop": false,
		"progressBar": false,
		"positionClass": "toast-bottom-right",
		"preventDuplicates": true,
		"onclick": null,
		"showDuration": "300",
		"hideDuration": "1000",
		"timeOut": timer,
		"extendedTimeOut": timer,
		"showEasing": "swing",
		"hideEasing": "linear",
		"showMethod": "fadeIn",
		"hideMethod": "fadeOut"
	};
	
	toastr[status](message, title);
}
/* SHOW NOTIFY TOASTER -- END*/

/* SET CARET AT THE END  
    set position of cursor at the end
*/
function SetCaretAtEnd(elem) {
    var elemLen = elem.value.length;
    // For IE Only
    if (document.selection) {
        // Set focus
        elem.focus();
        // Use IE Ranges
        var oSel = document.selection.createRange();
        // Reset position to 0 & then set at end
        oSel.moveStart('character', -elemLen);
        oSel.moveStart('character', elemLen);
        oSel.moveEnd('character', 0);
        oSel.select();
    }
    else if (elem.selectionStart || elem.selectionStart == '0') {
        // Firefox/Chrome
        elem.selectionStart = elemLen;
        elem.selectionEnd = elemLen;
        elem.focus();
    } // if
} // SetCaretAtEnd()
/* SET CARET AT THE END -- END */

function guid() {
	function s4() {
	    return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
	    s4() + '-' + s4() + s4() + s4();
};

function processRating(){
	$('.rating-list').each(function(){
		var rating = $(this);
		var ratingContainer = rating.closest('div');
		var ratingInput = ratingContainer.find('input.rating');
		var ratingVal = (ratingInput.val()) ? ratingInput.val() : 0;
		if(ratingVal){
			ratingContainer.find('.rating-list li').removeClass('active');
			ratingContainer.find('.rating-list li').removeClass('off');
			ratingContainer.find('.rating-list li span').removeClass('fa-star-half-o');
			ratingContainer.find('.rating-list li:nth-child('+Math.ceil(ratingVal)+')').addClass('active');
			if(ratingVal % 1 != 0){
				ratingContainer.find('.rating-list li:nth-child('+Math.ceil(ratingVal)+')').find('span').addClass('fa-star-half-o');
			}
		}else{
			ratingContainer.find('.rating-list li').addClass('off');
		}
	});
}

function changeRequirements(el, value) {
    if(el.is("input")) {
        el.not(".dummy").prop("required", value);    
    } else {
        el.find("input:not(.dummy)").prop("required", value);
    }
}

//--- color helpers
function colorToRGBA(color) {
    // Returns the color as an array of [r, g, b, a] -- all range from 0 - 255
    // color must be a valid canvas fillStyle. This will cover most anything
    // you'd want to use.
    // Examples:
    // colorToRGBA('red')  # [255, 0, 0, 255]
    // colorToRGBA('#f00') # [255, 0, 0, 255]
    var cvs, ctx;
    cvs = document.createElement('canvas');
    cvs.height = 1;
    cvs.width = 1;
    ctx = cvs.getContext('2d');
    ctx.fillStyle = color;
    ctx.fillRect(0, 0, 1, 1);
    return ctx.getImageData(0, 0, 1, 1).data;
}

function byteToHex(num) {
    // Turns a number (0-255) into a 2-character hex number (00-ff)
    return ('0'+num.toString(16)).slice(-2);
}

function colorToHex(color) {
    // Convert any CSS color to a hex representation
    // Examples:
    // colorToHex('red')            # '#ff0000'
    // colorToHex('rgb(255, 0, 0)') # '#ff0000'
    var rgba, hex;
    rgba = colorToRGBA(color);
    hex = [0,1,2].map(
        function(idx) { return byteToHex(rgba[idx]); }
        ).join('');
    return "#"+hex;
}

function validTextColour(stringToTest) {
    //Alter the following conditions according to your need.
    if (stringToTest === "") { return false; }
    if (stringToTest === "inherit") { return false; }
    if (stringToTest === "transparent") { return false; }

    var image = document.createElement("img");
    image.style.color = "rgb(0, 0, 0)";
    image.style.color = stringToTest;
    if (image.style.color !== "rgb(0, 0, 0)") { return true; }
    image.style.color = "rgb(255, 255, 255)";
    image.style.color = stringToTest;
    return image.style.color !== "rgb(255, 255, 255)";
}

//-- @addon : product addon object to be processed
//-- @order_item_addons : array of order_item_addons to compare to
//-- @order_item : parent of the addon that is being processed
function processAddOnsWithOrders(addon, order_item_addons, order_item) {
	var index = _.findIndex(order_item_addons, {order_item : order_item.id, addon_group : addon.id});
    addon.selected_subtotal = parseFloat(addon.price);
    addon.is_selected = 0;
    addon.selected_qty = 1;
    addon.selected_value = "";
    addon.selected_image_value = "";
    if(index > -1) {
        addon.is_selected = 1;
        addon.selected_qty = order_item_addons[index].qty;
        addon.selected_subtotal *= addon.selected_qty;
        addon.selected_value = order_item_addons[index].value;
        addon.selected_image_value = order_item_addons[index].image_value;
    }

    if(addon.type == "option") {
	    addon.items.map(function (option) {
	        var index = _.findIndex(order_item_addons, {order_item : order_item.id, addon_group : addon.id, addon_option : option.id});
	        if(index > -1) {
	            option.is_selected = 1;
	        }
	    });
	}

    return addon;
}

processRating();

// setTimeout(function() {
// 	// showNotifyToaster('success', 'Title Here', 'Lorem ipsum dolor sit amet!', 0);
// 	// showNotifyToaster('info', 'Info Here', 'Lorem ipsum dolor sit amet!', 0);
// 	// showNotifyToaster('warning', 'Warning Here', 'Lorem ipsum dolor sit amet!', 0);
// 	// showNotifyToaster('error', 'Error Here', 'Lorem ipsum dolor sit amet!', 0);
// }, 2000);

String.prototype.replaceAll = function (search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};