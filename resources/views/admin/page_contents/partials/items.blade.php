@if(count($content->clientItems) > 0 || $content->allow_add_items == 1)
<div class="caboodle-card">
	<div class="caboodle-card-header pad-top-15 pad-bottom-15">
		<div class="filters no-padding">
			<div class="flex align-center caboodle-form-control-connected">
				<h4 class="no-margin flex-1">
					{{ (@$view && @$view != '') ? @$view : 'Items' }}
				</h4>
				@if($content->allow_add_items == 1)
				<a href="{{route('adminClientPageContentItemCreate', [$page->slug, @$content->id])}}" class="btn-transparent btn-sm flex-1 text-right">
					<i class="far fa-plus-circle"></i> Add
				</a>
				@endif
			</div>
		</div>
	</div>
	<div class="caboodle-card-body">
		@if($content->allow_add_items == 1)
			<a href="{{route('adminClientPageContentItemCreate', [$page->slug, @$content->id])}}" class="{{$content->clientItems->count() > 0 ? 'hide' : ''}}">
				<div class="empty-message" role="button">
					Add an item here...
				</div>
			</a>
		@endif
		
				
		<table style="width:100%;">
			<tbody id="sortable" sortable-data-url="{{route('adminPageContentItemsOrder')}}">
				@foreach($content->clientItems as $item)
					<tr sortable-id="page_content_items-{{$item->id}}">
						<td>
							<div class="flex align-center caboodle-card bordered-style margin-bottom-10 pad-all-10">
								<i style="color:#00a09a;" class="mr-3 fa fa-th-large sortable-icon" aria-hidden="true"></i>
							
								<div class="flex-1 no-margin">
									@if($item->type == "default")
										@if($content->allow_add_items == 1)
											@if (@$item->controls->first()->type == "asset")
												<img src="{{asset(@$item->controls[0]->asset->path)}}" style="width: 100px; height: 80px; object-fit: contain;"/>
											@else
												<strong>{{@$item->controls->first()->value}}</strong>
											@endif
										@else
											<strong>{{$item->name}}</strong>
										@endif
									@else
										<strong>{{$item->type}}</strong>
									@endif
								</div>
								<div class="flex-auto">

									<a href="{{route('adminClientPageContentItemEdit', [$page->slug, $item->content_id, $item->id])}}">
										@if($item->editable == 1)
											<i class="far fa-edit hover-scale color-primary" role="button" data-toggle="tooltip" title="Edit"></i>
										@else
											<i class="far fa-external-link hover-scale color-primary" role="button" data-toggle="tooltip" title="View"></i>
										@endif
									</a>

									&nbsp;
									@if($content->allow_add_items == 1)
										<a href="{{ route('adminClientPageContentItemDestroy', $item->id) }}" class="item-delete" data-id="{{ $item->id }}">
											<i class="far fa-trash hover-scale color-red" role="button" data-toggle="tooltip" title="Delete"></i>
										</a>
									@endif
								</div>
							</div>
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@endif