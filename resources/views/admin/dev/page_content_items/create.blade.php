@extends('layouts.admin')

@section('breadcrumbs')
<nav aria-label="breadcrumb">
	<ol class="breadcrumb">
		<li class="breadcrumb-item far">
			<a href="{{route('adminDashboard')}}">Dashboard</a>
		</li>
		<li class="breadcrumb-item far">
			<span>Dev Modules</span>
		</li>
		<li class="breadcrumb-item far">
			<a href="{{route('adminPages')}}">Pages</a>
		</li>
		<li class="breadcrumb-item far">
			<a href="{{ route('adminPagesEdit', [$content->pageGroup->id]) }}">Edit {{ $content->pageGroup->name }}</a>
		</li>
		<?php
		// <li class="breadcrumb-item far">
		// 	<a href="{{ route('adminPageEdit', $content->name) }}">Edit {{ $content->name }}</a>
		// </li> 
		?>
		<li class="breadcrumb-item far">
			<a href="{{ route('adminPageContentsEdit', $content->id) }}">Edit {{ $content->name }}</a>
		</li>
		<li class="breadcrumb-item far active">
			<span>Create Item</span>
		</li>
	</ol>
</nav>
@stop

@section('header')
<header class="flex-center">
    <h1>{{ $title }}</h1>
    <div class="header-actions">
		<a class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple" href="{{ route('adminPageContentsEdit', $content->id) }}">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</header>
@stop 

@section('footer')
<footer>
    <div class="text-right">
        <a class="caboodle-btn caboodle-btn-large caboodle-btn-cancel mdc-button" data-mdc-auto-init="MDCRipple" href="{{ route('adminPageContentsEdit', $content->id) }}">Cancel</a>
        <button class="caboodle-btn caboodle-btn-large caboodle-btn-primary mdc-button mdc-button--unelevated form-parsley-submit" data-mdc-auto-init="MDCRipple">Save</button>
    </div>
</footer>
@stop

@section('content')
  {!! Form::open(['route'=>'adminPageContentItemsStore', 'class'=>'form form-parsley form-create form-clear']) !!}
    @include('admin.dev.page_content_items.form')
  {!! Form::close() !!}
@stop