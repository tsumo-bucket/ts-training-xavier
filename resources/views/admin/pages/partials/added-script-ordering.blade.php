<script>
    	$( "#sortable" ).sortable({
      placeholder: ".sortable tr",
      cursor: "move",
      update: function( event, ui ) {
      	//puts the sortable ID in an array
      	var order = $(this).sortable('serialize', {
                attribute: 'sortable-id',//this will look up this attribute
                // key: 'order',//this manually sets the key
			});
      	//foreach list of array in order
      	var routeURL= $(this).attr('sortable-data-url');
		// console.log(order);
      		updateSortableTable(routeURL,order);
	    }
    });
	 
	//AJAX for Sortable Tables
	function updateSortableTable(url, data) {
		$.ajax({
			type : 'get',
			url: url,
			data : data,
			dataType : 'json',
			processData: false,
			success : function(data) {
				// alert();
				var title = data.notifTitle;
				var message = (_.isUndefined(data.notifMessage) ? '' : data.notifMessage);
				showNotifyToaster('info', data.notifTitle);

				if (data.resetForm) {
					resetForm(parselyForm);
				}

				if (! _.isUndefined(data.redirect)) {
					setTimeout(function() {
						window.location = data.redirect;
					}, 2500);
				}
			},
			error : function(data, text, error) {
				var message = '';
				_.each(data.responseJSON, function(val) {
					message += val + ' ';
				});
				// showNotify('Error saving.', message);
				// console.log(message)
			}
		});
	};

</script>